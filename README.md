# nginx-wordpress
A light Nginx container utilizing microcaching to serve single site Wordpress PHP-FPM containers.

# Docker run command:
```
$ docker run -d -p 80:80 -p 443:443 -e PHP_FPM_SOCK=php-fpm:9000 -e SERVER_NAME=<server-FQDN> -v $(pwd)/var/www/html:/var/www/html --link <your-fpm-container-name>:php-fpm myvbo/nginx-wordpress
```

# Notes:
The Container expects you are running a php-fpm container with port 9000 exposed at the container level. No need to port it out to the host.

Microcaching is on by default, set to a very modest 20s validity. The cache is circumvented on all Woocommerce pages and admin. Logged in users will not benefit from cache.