FROM nginx:alpine
MAINTAINER Paul Carrier <paul@airtank.com>

COPY default.tmp /etc/nginx/conf.d/default.tmp
ADD global /etc/nginx/global/

CMD envsubst '\$PHP_FPM_SOCK:$SERVER_NAME' < /etc/nginx/conf.d/default.tmp > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'